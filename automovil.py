from vehiculo import Vehiculo


class Automovil(Vehiculo):
    '''
    Esta clase representa un "Automovil"
    '''
    def __init__(self, placa):
        super().__init__(placa=placa)

    def obtenerCosto(self) -> int:
        return 1_000

