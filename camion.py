from vehiculo import Vehiculo


class Camion(Vehiculo):
    '''
    Esta clase representa un "Camión"
    '''
    def __init__(self, placa: str):
        super().__init__(placa)

    def obtenerCosto(self) -> int:
        return 2_000

