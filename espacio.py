import math
from typing import Tuple

from vehiculo import Vehiculo


class Espacio:
    '''
    Esta clase describe cada uno de los "Espacio" dentro del parqueo
    '''
    def __init__(self, numero:int):
        self.id:int = numero
        self.vehiculo: Vehiculo = None
        self.hora_inicio:float = 0.0

    def esta_placa(self, placa:str) -> bool:
        '''
        Este método verifica si hay un vehículo, en caso que haya uno
        verifica que tenga la placa que se esta buscando
        :param placa: Número de placa a buscar
        :return: True si hay vehículo y es la placa, False si no hay vehículo o si hay
        vehículo con placa diferente
        '''
        encontro_placa = False

        if not self.disponible():
            # Si hay un vehículo, revisa la placa
            # compara la placa en igualdad de formato
            encontro_placa =  (self.vehiculo.placa.upper() == placa.upper())

        return encontro_placa

    def disponible(self) -> bool:
        '''
        Este método indica si el espacio está disponible para parquear
        :return: Devuelve True si esta libre, caso contrario False
        '''
        return self.vehiculo is None

    def parquear(self, v:Vehiculo, h:float) -> bool:
        '''
        Este método se utiliza para parquear un vehículo dentro del espacio,
        antes de aplicar la operación verifica si el espacio está disponible.
        En caso que este libre y puede estacionar devuelve un True, en caso contrario
        devuelve un False
        :param v: Vehículo que se desea parquear
        :param h: Hora de ingreso al parqueo, para luego se utilizada para deteminar
        el tiempo transcurrido
        :return: Devuevle True si la operación se puede realizar, caso contrario False
        '''
        if self.disponible():
            self.vehiculo = v
            self.hora_inicio = h
            return True
        else:
            return False

    def retirar(self, hora_salida:float) -> Tuple[Vehiculo, float, float, float]:
        '''
        Este método es que hace la lógica necesaria parar retirar el vehículo del espacio.
        Además, realiza los cálculos para determinar los tiempos, costo y vehículo
        :param hora_salida:
        :return: (Vehículo, monto a cobrar, tiempo cobrado, tiempo transcurrido)
        '''

        if not self.disponible():
            # NO disponible, significa que hay un vehículo
            tiempo = hora_salida - self.hora_inicio
            if tiempo < 1:
                tiempo_cobrar = 1
            else:
                tiempo_cobrar = int(tiempo)
                fraccion = tiempo - int(tiempo)
                if fraccion <= 0.25:
                    tiempo_cobrar = tiempo_cobrar + 0.25
                elif 0.25 < fraccion <= 0.5:
                    tiempo_cobrar = tiempo_cobrar + 0.5
                elif 0.5 < fraccion <= 0.75:
                    tiempo_cobrar = tiempo_cobrar + 0.75
                else:
                    tiempo_cobrar = tiempo_cobrar + 1

            monto_por_cobrar = tiempo_cobrar * self.vehiculo.obtenerCosto()
            vehiculo_por_retirar = self.vehiculo
            self.vehiculo = None
            return vehiculo_por_retirar, monto_por_cobrar, tiempo_cobrar, tiempo
        else:
            # Disponible, signfica que NO hay un vehículo
            # Por lo tanto devuelve valores en "cero"
            return None, 0, 0, 0

