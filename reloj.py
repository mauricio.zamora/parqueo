from typing import Tuple


class Reloj:
    '''
    Esta clase define el "reloj" usado para controlar el tiempo del parqueo
    '''
    def __init__(self):
        # __ al inicio significa que es "privado" que no debería de usarse directo
        self.__hora:int = 12
        self.__minutos:int = 0

    # Las propiedades son la forma de como controlar los valores de los atributos
    # para asegurnos que no puedan tener valores inválidos
    @property
    def hora(self) -> int:
        '''
        Devuelve la hora (solamente el valor de 0 a 23)
        :return: Hora [0..23]
        '''
        return self.__hora

    @hora.setter
    def hora(self, valor) -> None:
        '''
        Establece la hora del reloj
        :param valor: Valor para hora entre 0 y 23
        '''
        if 0 <= valor <= 23:
            self.__hora = valor

    @property
    def minutos(self) -> int:
        '''
        Devuelve los minutos del reloj entre 0 y 59
        :return: Minutos [0..59]
        '''
        return self.__minutos

    @minutos.setter
    def minutos(self, valor) -> None:
        '''
        Establece los minutos del reloj
        :param valor: Valor para hora entre 0 y 59
        '''
        if 0 <= valor < 60:
            self.__minutos = valor

    def mostrar_hora(self) -> str:
        '''
        Devuelve la hora en formato hh:mm
        :return:
        '''
        return '{:d}:{:02d}'.format(self.hora, self.minutos)

    def obtener_tiempo(self) -> float:
        '''
        Obtiene el tiempo en formato decimal
        :return: Valor decimal equivalente de la hora
        '''
        return self.hora + (self.minutos/60)

    def sumar_minutos(self, minutos) -> None:
        '''
        Este método suma minutos a la hora actual, si la cantidad de minutos
        superar la hora, se suman las horas respectivas
        :param minutos: Cantidad de minutos a sumar
        '''
        total_minutos = self.minutos + minutos
        self.minutos = total_minutos % 60
        total_horas = total_minutos // 60
        self.sumar_horas(total_horas)

    def sumar_horas(self, horas) -> None:
        '''
        Este método suma la cantidad de horas a la hora
        :param horas: Cantidad de horas a sumar
        '''
        #suma tantas horas, como horas que queremos agregar
        for i in range(horas):
            self.sumar_hora()

    def sumar_hora(self) -> None:
        '''
        Este método suma una hora al tiempo actual
        '''
        # si son las 23 horas y suma 1 hora, se reinicia a las 0 horas
        if self.hora == 23:
            self.hora = 0
        else:
            self.hora = self.hora + 1
