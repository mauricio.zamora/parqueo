class Vehiculo:
    def __init__(self, placa:str):
        self.placa:str = placa

    def obtenerCosto(self) -> int:
        '''
        Obtiene el monto del costo por hora, según el tipo
        por lo que cada subtipo tiene la responsabilidad de
        establecer el costo
        :return: Costo de cada subtipo
        '''
        #pass significa que lo deje incompleto y luego se debe implementar
        pass