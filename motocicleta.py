from vehiculo import Vehiculo


class Motocicleta(Vehiculo):
    def __init__(self, placa: str):
        super().__init__(placa)

    def obtenerCosto(self) -> int:
        return 500

