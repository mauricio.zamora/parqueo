from typing import List, Tuple, Optional

from espacio import Espacio
from reloj import Reloj
from vehiculo import Vehiculo


class Parqueo:
    '''
    Esta clase describe un "Parqueo"
    '''

    def __init__(self, cantidad_espacios: int):
        self.espacios: List[Espacio] = []
        self.historial: List[Tuple[int, float, float, float]] = []
        self.reloj: Reloj = Reloj()
        for i in range(1, cantidad_espacios + 1):
            self.espacios.append(Espacio(i))

    def parquear(self, v: Vehiculo) -> bool:
        '''
        Este método parquea un vehículo en el primer espacio disponible,
        establece la hora de ingreso al espacio.  Si la operación exitosa
        devuelve un True, si no encuentrá un espacio, devuelve un False
        :param v: Vehículo que se desea parquear
        :return: True si logró parquear el vehículo, False si no lo logró
        '''
        operacion_exitosa = False
        # Busca uno a uno en los espacios para encontrar
        # el primera espacio libre para parquear
        for espacio in self.espacios:
            if espacio.disponible():
                # Si entró acá signfica que encontró un espacio disponible
                espacio.parquear(v, self.reloj.obtener_tiempo())
                operacion_exitosa = True
                # break sale del ciclo
                break

        return operacion_exitosa

    def retirar(self, placa: float) -> Tuple[Optional[Vehiculo], float, float, float, int]:
        '''
        Buscar un vehículo por su número de placa, una vez que lo encuentra lo retira
        y realiza los cálculos correspondientes.
        :param placa:
        :return: (Vehículo, monto a cobrar, tiempo cobrado, tiempo transcurrido, numero_espacio)
        '''
        espacio: Espacio = None
        # primer paso, buscar la placa en cada espacio, si la encuentra procesar el espacio
        for e in self.espacios:
            if e.esta_placa(placa):
                espacio = e
                # sale del ciclo
                break

        if espacio is not None:
            # si encontró el espacio con la placa, entonces hay un objeto espacio
            vehiculo_por_retirar, monto_por_cobrar, tiempo_cobrar, tiempo = espacio.retirar(self.reloj.obtener_tiempo())
            self.historial.append((espacio.id, monto_por_cobrar, tiempo_cobrar, tiempo))
            return vehiculo_por_retirar, monto_por_cobrar, tiempo_cobrar, tiempo, espacio.id
        else:
            return None, 0.0, 0.0, 0.0, 0

    def disponible(self) -> bool:
        '''
        Indica si hay al menos un "Espacio" disponible dentro del parqueo
        :return: True si hay al menos un espacio disponible, si está lleno False
        '''
        return any([e.disponible() for e in self.espacios])

    def cantidad_espacios_disponibles(self) -> int:
        '''
        Devuelve la cantidad de espacios disponibles en el parqueo
        :return: Cantidad de espacios disponibles
        '''
        return sum([e.disponible() for e in self.espacios])

    def cantidad_espacios_ocupados(self) -> int:
        '''
        Devuelve la cantidad de espacios ocupados en el parqueo
        :return: Cantidad de espacios ocupados
        '''
        return len(self.espacios) - self.cantidad_espacios_disponibles()
