# Ejemplo del parqueo

Este ejemplo se creo utilizando la siguiente prosa inicial:

Se necesita crear una aplicación para administrar lotes de parqueos en San José, cada lote de parqueos tiene una capacidad limitada de vehículos que puede albergar. Entre estos se tiene automóviles, camiones, motocicletas. Una característica relevante es que se cobra por hora según el tipo de vehículos, aplicando la regla, que se cobra al menos una hora, luego cuartos de hora.
El proceso de funcionamiento del parqueo son los siguientes: 
1. El parqueo habilita la entrada de vehículos si tiene capacidad.
1. Se parquea el vehículo en el primer espacio disponible. Se registra la hora en el momento que parquea.
1. Se debe registrar la placa del vehículo.
1. Se imprime un tiquete al ingreso, este tiquete se usa para pagar los montos.
1. El sistema debe indicar la cantidad de espacios disponible. 
1. El sistema debe llevar un registro del dinero obtenido, así como estadística de los tipos de vehículo.


```
cd existing_repo
git remote add origin https://gitlab.com/mauricio.zamora/parqueo.git
git branch -M main
git push -uf origin main
```

